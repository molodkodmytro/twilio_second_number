require 'twilio-ruby'

module MyTwilio
  module Client
    def client
      @client ||= Twilio::REST::Client.new(ENV["TWILIO_ACCOUNT_SID"], ENV["TWILIO_AUTH_TOKEN"])
    end

    module_function :client
  end
end