Rails.application.routes.draw do
  namespace :api, defaults: { format: "json" } do
    namespace :v1 do
      resources :countries, only: :index
      resources :phones, only: %w(index create) do
        collection do
          get :my_phones
        end
      end
      resources :areas, only: %w(index)
      resources :chat_rooms, only: %w(index show create)
      resources :messages, only: %w(show) do
        collection do
          post :outbound_create
          post :inbound_create
          post :status_callback
        end
      end
      resources :subscriptions, only: %w(index) do
        collection do
          get :info
          get :priority
        end
      end

      resources :profile, only: [] do 
        collection do 
          post :firebase
        end
      end
    end
  end
end
