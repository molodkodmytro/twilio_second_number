class Api::ApiController < ActionController::API
  include JsonMessagesHelper
  
  before_action :authentication!
  
  def current_user
    @current_user
  end

  private

  def authentication!
    pp "=" * 100
    pp request.headers["udid"]
    pp "=" * 100

    result = ::V1::Authentication::Auth.call(
      udid: request.headers["udid"]
    )
    if result.success?
      @current_user = result.user
    else
      render json: { error: result.message }, status: 401
      return
    end
  end
end
