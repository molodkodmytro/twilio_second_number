class Api::V1::PhonesController < Api::ApiController
  def index
    result = ::V1::Phones::GetPhones.call(search_params)
    if result.success?
      render json: { number: result.phones }
    else
      render json: { error: result.message }, status: 400
    end
  end

  def create
    twilio_phone = MyTwilio::Client.client.incoming_phone_numbers.create(
      phone_number: phone_params[:number],
      sms_url: ENV["HOST"] + ENV["TWILIO_SMS_INBOUND"],
      voice_url: ENV['TWILIO_CALLS_INBOUND']
    )

    phone = current_user.phones.new(
      number: twilio_phone.phone_number,
      phone_type: phone_params[:phone_type],
      capabilities: twilio_phone.capabilities
    )

    if phone.save
      $redis.del(@id)
      render json: { phone: phone }
    else
      render json: { error: phone.errors }, status: 400
    end
  rescue Twilio::REST::RestError => e
    render json: { error: e.message }, status: 400
  end

  def my_phones
    @phones = current_user.phones
  end

  private

  def phone_params
    @id ||= params.require(:number_id)
    return JSON.parse($redis.get(@id), symbolize_names: true)
  end

  def search_params
    country_code = params.require(:country_code)
    params.permit(:query, :type, :region_code).merge(country_code: country_code)
  end
end
