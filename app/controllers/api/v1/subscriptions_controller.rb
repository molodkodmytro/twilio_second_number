class Api::V1::SubscriptionsController < Api::ApiController
  def index
    @plans = {}
    @plans[:unlimited_plans] = Plan.where(time: 0)
    @plans[:prepaid_plans] = Plan.where.not(time: 0)
  end

  def priority
    @subscriptions = current_user.user_plans
      .includes(:plan)
      .where('plans.is_priority': true)
      .take(3)
  end

  def info
    set_subscription
  end

  private

  def set_subscription
    @subscription = current_user.user_plans.order(:created_at).last
  end
end
