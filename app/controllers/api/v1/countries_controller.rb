class Api::V1::CountriesController < Api::ApiController
  def index
    @res = Country.order(:country)

    @countries = {}
    @countries[:most_popular] = @res.where(most_popular: true)
    @countries[:all] = @res.where(most_popular: false)
  end

  private

  def country_params
    params.permit(:query)
  end
end
