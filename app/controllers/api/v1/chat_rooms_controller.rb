class Api::V1::ChatRoomsController < Api::ApiController
  def index
    @chat_rooms = current_user.chat_rooms.joins(:messages).group("chat_rooms.id")
      .order(updated_at: :desc).page(params[:page])
  end

  def show
    @chat_room = current_user.chat_rooms
      .find(params[:id])
      
    @messages = @chat_room.messages
      .order(created_at: :desc)
      .page(params[:page])
      .per(params[:per_page])
  end

  def create
    @chat_room = set_sender.from_chat_rooms.where(
      second_phone: set_receiver
    ).first_or_create!
  end

  private

  def set_sender
    @sender ||= current_user.phones.find_by!(number: params[:sender_number])
  end

  def set_receiver
    @receiver ||= Phone.where(number: params[:receiver_number]).first_or_create! do |phone|
      phone.number     = params[:receiver_number]
      phone.phone_type = 3
    end
  end

  def chat_room_params
    params.permit(:name, :sender_number, :receiver_number)
  end
end
