class Api::V1::AreasController < Api::ApiController
  def index
    @type = area_params[:type].to_i
    @areas = Area.where(country_code: area_params[:country_code]).
      where("name ILIKE ?", "%#{area_params[:query]}%")
  end

  private

  def area_params
    params.permit(:country_code, :query, :type)
  end
end
