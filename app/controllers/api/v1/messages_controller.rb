class Api::V1::MessagesController < Api::ApiController
  skip_before_action :authentication!, only: %I(inbound_create status_update status_callback)

  def outbound_create
    result = ::V1::Messages::OutboundCreate.call(
      chat_room_id: message_params[:chat_room_id],
      from:         message_params[:from],
      text:         message_params[:text],
      current_user: current_user,
      file:         message_params[:file]
    )
    if result.success?
      @message = result.sms
    else
      pp "=" * 100
      pp json_service_messages(result)
      pp "=" * 100
      
      render json_service_messages(result)
    end
  end

  def inbound_create
    result = ::V1::Messages::InboundCreate.call(
      from:        inbound_params[:From],
      to:          inbound_params[:To],
      text:        inbound_params[:Body],
      message_sid: inbound_params[:MessageSid]
    )

    render json_service_messages(result) unless result.success?
  end

  def show
    set_message
  end

  def status_callback
    result = ::V1::Messages::StatusUpdate.call(
      message_sid: inbound_params[:MessageSid],
      to:          inbound_params[:To],
      status:      inbound_params[:MessageStatus]
    )

    render json_service_messages(result) unless result.success?
  end
  
  private

  def set_message
    @message ||= Message.find(message_params[:id])
  end

  def message_params
    params.permit(:id, :text, :chat_room_id, :from, :file)
  end

  def find_to_phone
    @phone ||= Phone.find_by(number: inbound_params[:To])
  end

  def inbound_params
    params.permit(:From, :To, :Body, :MessageStatus, :MessageSid)
  end
end
