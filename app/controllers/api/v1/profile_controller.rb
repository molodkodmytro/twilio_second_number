class Api::V1::ProfileController < Api::ApiController
  def firebase
    if current_user.update(firebase_id: firebase_params[:firebase_token])
      render json: { success: true }
    else
      render json: { errors: current_user.errors }
    end
  end

  private

  def firebase_params
    params.permit(:firebase_token)
  end
end