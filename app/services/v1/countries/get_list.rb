class V1::Countries::GetList
  extend LightService::Organizer

  def self.call(query:)
    with(
      query:              query,            
      formated_countries: { most_popular: [], all: [] },
      twilio_client:      MyTwilio::Client.client
    ).reduce(
      V1::Countries::GetList::GetCountryListFromTwilio,
      V1::Countries::GetList::SearchCountriesByQuery,
      V1::Countries::GetList::FormatAndSortList,
      V1::Countries::GetList::SortCountriesByPopularity
    )
  end

  class V1::Countries::GetList::GetCountryListFromTwilio
    extend LightService::Action

    expects  :twilio_client
    promises :twilio_countries

    executed do |context|
      context.twilio_countries = context.twilio_client.api.available_phone_numbers.list
    end
  end
  
  class V1::Countries::GetList::SearchCountriesByQuery
    extend LightService::Action

    expects  :twilio_countries, :query
    promises :twilio_countries

    executed do |context|
      next unless context.query.present?

      countries = context.twilio_countries.select do |country|
        country.country =~ /#{context.query}/i
      end

      context.twilio_countries = countries
    end
  end

  class V1::Countries::GetList::FormatAndSortList
    extend LightService::Action

    expects  :twilio_countries
    promises :countries

    executed do |context|
      countries = context.twilio_countries.map do |country|
        { 
          name:        country.country, 
          code:        country.country_code, 
          flag_url:    image_url(country.country_code),
          mobile_code: mobile_code(country.country_code)
        }
      end

      context.countries = countries.sort {|a, b| a[:name] <=> b[:name]}
    end

    def self.mobile_code(country_code)
      IsoCountryCodes.find(country_code).calling
    end

    def self.image_url(country_code)
      "https://www.countryflags.io/#{country_code}/flat/64.png"
    end
  end

  class V1::Countries::GetList::SortCountriesByPopularity
    extend LightService::Action

    expects  :countries, :formated_countries
    promises :formated_countries

    executed do |context|
      context.countries.each do |country|
        if Country::MOST_POPULAR.include?(country[:code])
          context.formated_countries[:most_popular] << country
        else
          context.formated_countries[:all] << country
        end
      end
    end
  end
end
 