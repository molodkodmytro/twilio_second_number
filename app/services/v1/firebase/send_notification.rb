class V1::Firebase::SendNotification
  extend LightService::Organizer
  require 'fcm'

  def self.call(to_phone:, sms:)
    with(
      to_phone: to_phone,
      sms:      sms,
      fcm:      FCM.new(ENV["FIREBASE_SERVER_KEY"])
    ).reduce(
      V1::Firebase::SendNotification::Perform
    )
  end

  class V1::Firebase::SendNotification::Perform
    extend LightService::Action

    expects  :to_phone, :sms, :fcm
    promises :response

    executed do |context|
      options = { 
        notification: {
          data:{
            message_id: context.sms.id,
            chat_room_id: context.sms.chat_room.id
          },
          title: "New message for #{context.to_phone.number}",
          body: context.sms.text
        }
      }
      registration_id = context.to_phone.user.firebase_id
      context.response = context.fcm.send([registration_id], options)

      pp "=" * 100
      pp context.response
      pp "=" * 100
      pp options
      pp "=" * 100
    end
  end
end