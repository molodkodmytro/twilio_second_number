class V1::Messages::InboundCreate
  extend LightService::Organizer

  def self.call(from:, to:, text:, message_sid:)
    with(
      text:        text,
      from_number: from,
      to_number:   to,
      message_sid: message_sid
    ).reduce(
      V1::Messages::InboundCreate::FindSender,
      V1::Messages::InboundCreate::FindReceiver,
      V1::Messages::InboundCreate::FindConversation,
      V1::Messages::InboundCreate::BuildMessage,
      V1::Messages::InboundCreate::SaveMessage,
      V1::Messages::InboundCreate::SendNotification
    )
  end

  class V1::Messages::InboundCreate::FindSender
    extend LightService::Action

    expects  :from_number
    promises :from_phone

    executed do |context|
      context.from_phone = Phone.where(number: context.from_number).first_or_create! do |phone|
        phone.number = context.from_number
      end
    end
  end
  
  class V1::Messages::InboundCreate::FindReceiver
    extend LightService::Action

    expects  :to_number
    promises :to_phone

    executed do |context|
      context.to_phone = Phone.find_by!(number: context.to_number)
    end
  end

  class V1::Messages::InboundCreate::FindConversation
    extend LightService::Action

    expects  :from_phone, :to_phone
    promises :chat_room

    executed do |context|
      context.chat_room = ChatRoom.where(first_phone_id: context.from_phone.id, second_phone_id: context.to_phone.id).or(
        ChatRoom.where(first_phone_id: context.to_phone.id, second_phone_id: context.from_phone.id)
      ).take

      next if context.chat_room.present?

      context.chat_room = ChatRoom.create(first_phone_id: context.from_phone.id, second_phone_id: context.to_phone.id)
    end
  end

  class V1::Messages::InboundCreate::BuildMessage
    extend LightService::Action

    expects  :chat_room, :text, :to_phone, :message_sid, :from_number
    promises :sms

    executed do |context|
      context.sms = context.to_phone.user.messages.new(
        chat_room:   context.chat_room, 
        text:        context.text,
        phone:       context.to_phone,
        message_sid: context.message_sid
      )
    end
  end

  class V1::Messages::InboundCreate::SaveMessage
    extend LightService::Action

    expects :sms, :from_phone

    executed do |context|
      next if context.from_phone.user.present?

      unless context.sms.save
        context.fail_and_return!(context.message.errors.full_messages)
      end
    end
  end

  class V1::Messages::InboundCreate::SendNotification
    extend LightService::Action

    expects :to_phone, :sms, :from_phone

    executed do |context|
      next if context.from_phone.user.present?

      result = V1::Firebase::SendNotification.call(
        to_phone: context.to_phone,
        sms:      context.sms
      )
      unless result.success?
        context.fail_and_return!(result.message)
      end
    end
  end
end