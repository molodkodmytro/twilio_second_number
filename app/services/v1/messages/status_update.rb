class V1::Messages::StatusUpdate
  extend LightService::Organizer

  def self.call(message_sid:, to:, status:)
    with(
      status:      status,
      to_number:   to,
      message_sid: message_sid
    ).reduce(
      V1::Messages::StatusUpdate::FindMessageBySid,
      V1::Messages::StatusUpdate::FindToPhone,
      V1::Messages::StatusUpdate::UpdateMessageStatus,
      V1::Messages::StatusUpdate::CheckMessageStatus,
      V1::Messages::StatusUpdate::SendNewMessageNotification
    )
  end

  class V1::Messages::StatusUpdate::FindMessageBySid
    extend LightService::Action

    expects  :message_sid, :status
    promises :sms_message

    executed do |context|
      context.sms_message = Message.find_by(message_sid: context.message_sid)

      pp "=" * 100
      pp context.status
      pp "=" * 100
      pp context.sms_message.present?
      pp "=" * 100
    end
  end

  class V1::Messages::StatusUpdate::FindToPhone
    extend LightService::Action

    expects  :to_number
    promises :to_phone

    executed do |context|
      context.to_phone = Phone.find_by(number: context.to_number)
    end
  end

  class V1::Messages::StatusUpdate::UpdateMessageStatus
    extend LightService::Action

    expects  :sms_message, :status
    promises :sms_message

    executed do |context|
      next if context.status.eql?("sent")

      context.sms_message&.update(status: context.status)
    end
  end

  class V1::Messages::StatusUpdate::CheckMessageStatus
    extend LightService::Action

    expects  :sms_message
    promises :sms_message

    executed do |context|
      next unless context.sms_message.delivered?

      context.sms_message.touch(:delivered_at)
    end
  end

  class V1::Messages::StatusUpdate::SendNewMessageNotification
    extend LightService::Action

    expects  :sms_message, :to_phone

    executed do |context|
      next unless context.sms_message.delivered?

      result = V1::Firebase::SendNotification.call(
        to_phone: context.to_phone,
        sms:      context.sms_message
      )
      unless result.success?
        context.fail_and_return!(result.message)
      end
    end
  end
end