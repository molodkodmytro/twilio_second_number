class V1::Messages::OutboundCreate
  extend LightService::Organizer

  def self.call(from:, chat_room_id:, text:, current_user:, file:)
    with(
      twilio_client: MyTwilio::Client.client,
      current_user:  current_user,
      chat_room_id:  chat_room_id,
      from_number:   from,
      text:          text,
      file:          file
    ).reduce(
      V1::Messages::OutboundCreate::FindConversation,
      V1::Messages::OutboundCreate::FindReceiver,
      V1::Messages::OutboundCreate::BuildMessage,
      V1::Messages::OutboundCreate::SendMessage,
      V1::Messages::OutboundCreate::SaveMessage
    )
  end

  class V1::Messages::OutboundCreate::FindConversation
    extend LightService::Action

    expects  :chat_room_id, :current_user
    promises :chat_room

    executed do |context|
      context.chat_room = context.current_user.chat_rooms
        .find_by(id: context.chat_room_id)

      if context.chat_room.nil?
        context.fail_and_return!("You aren't member of this chat room!")
      end
    end
  end

  class V1::Messages::OutboundCreate::FindReceiver
    extend LightService::Action

    expects  :chat_room, :from_number
    promises :to

    executed do |context|
      context.to = context.chat_room.receiver(context.from_number)
    end
  end

  class V1::Messages::OutboundCreate::BuildMessage
    extend LightService::Action

    expects  :from_number, :chat_room, :text, :current_user, :file
    promises :sms

    executed do |context|
      context.sms = context.current_user.messages.new(
        chat_room: context.chat_room, 
        text: context.text,
        file: context.file,
        phone: context.chat_room.sender(context.from_number)
      )
    end
  end

  class V1::Messages::OutboundCreate::SendMessage
    extend LightService::Action

    expects :twilio_client, :sms, :from_number, :to, :text
    promises :result_sms

    executed do |context|
      begin
        context.result_sms = context.twilio_client.messages.create(sms_params(context))
      rescue Twilio::REST::RestError => e
        pp "=" * 100
        pp e.error_message
        pp "=" * 100
        
        context.fail_and_return!(e.error_message)
      end

      pp "=" * 100
      pp context.result_sms
      pp "=" * 100
    end

    def self.sms_params(context)
      res = {
        from: context.from_number,
        body: context.text,
        to: context.to.number,
        status_callback: ENV['HOST'] + ENV['STATUS_CALLBACK']
      }

      res[:media_url] = context.sms.file.url if context.sms.file.url.present?
      res
    end
  end

  class V1::Messages::OutboundCreate::SaveMessage
    extend LightService::Action

    expects  :sms

    executed do |context|
      unless context.sms.save
        context.fail_and_return!(context.sms.errors.full_messages)
      end
      context.sms.reload
    end
  end

  class V1::Messages::OutboundCreate::SaveMessage
    extend LightService::Action

    expects  :sms, :result_sms

    executed do |context|
      context.sms.update(message_sid: context.result_sms.sid)
    end
  end
end
