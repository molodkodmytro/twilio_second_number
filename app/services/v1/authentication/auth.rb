class V1::Authentication::Auth
  extend LightService::Organizer

  def self.call(udid)
    with(
      udid: udid[:udid]
    ).reduce(
      V1::Authentication::Auth::CryptUdid,
      V1::Authentication::Auth::CheckUser,
    )
  end

  class V1::Authentication::Auth::CryptUdid
    extend LightService::Action

    expects  :udid
    promises :encrypted_udid

    executed do |context|
      context.fail_and_return!("Udid does not present") if context.udid.blank?
      context.fail_and_return!("Udid does not pass validate") unless validate_udid(context.udid)

      context.encrypted_udid = hexdigest(context.udid)
    end

    def self.validate_udid(udid)
      # TODO: set valid udid validation
      udid.length >= 5
    end

    def self.hexdigest(udid)
      Digest::SHA256.hexdigest(udid.to_s + secure_salt)
    end

    def self.secure_salt
      Digest::MD5.hexdigest(
        Rails.application.secrets.secret_key_base.to_s
      )
    end
  end

  class V1::Authentication::Auth::CheckUser
    extend LightService::Action

    expects  :encrypted_udid
    promises :user

    executed do |context|
      context.user = User.where(udid: context.encrypted_udid).first_or_create!
    end
  end
end
