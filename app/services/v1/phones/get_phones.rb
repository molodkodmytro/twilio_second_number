class V1::Phones::GetPhones
  extend LightService::Organizer

  def self.call(search_params)
    with(
      country_code:       search_params[:country_code],
      query:              search_params[:query],
      type:               search_params[:type],
      region_code:        search_params[:region_code],
      twilio_client:      MyTwilio::Client.client
    ).reduce(
      V1::Phones::GetPhones::CheckType,
      V1::Phones::GetPhones::CreateQuery,
      V1::Phones::GetPhones::TwilioGet
    )
  end

  class V1::Phones::GetPhones::CheckType
    extend LightService::Action

    expects  :type
    promises :type

    executed do |context|
      context.type = Phone::PHONE_TYPES[context.type.to_i] || :toll_free
    end
  end

  class V1::Phones::GetPhones::CreateQuery
    extend LightService::Action

    expects  :query
    promises :query

    executed do |context|
      next if context.query.blank? || context.query.size >= 3
      context.query = context.query + "*" * (3 - context.query.size)
    end
  end

  class V1::Phones::GetPhones::TwilioGet
    extend LightService::Action

    expects  :twilio_client, :query, :country_code, :region_code, :type
    promises :phones

    executed do |context|
      context.phones = context.twilio_client.api
        .available_phone_numbers(context.country_code)
        .send(context.type)
        .list(list_params(context))

      context.phones.map! { |el| set_number(el, context) }
    rescue Twilio::REST::RestError => e
      context.phones = []
    end

    def self.set_number(number, context)
      id = SecureRandom.uuid
      $redis.set(id, phone_params(number, context).to_json, ex: 600)
      {
        id: id,
        number: number.phone_number,
        capabilities: capabilities(number)
      }
    end

    def self.phone_params(number, context)
      { 
        number: number.phone_number,
        phone_type: context.type
      }
    end

    def self.capabilities(number)
      number.capabilities.inject({}) do |memo, (key, value)|
        memo[key.downcase] = value
        memo
      end
    end

    def self.list_params(context)
      {
        area_code: context.region_code, 
        contains: "#{context.query}", 
        voice_enabled: true,
        sms_enabled: true
      }
    end
  end
end
 