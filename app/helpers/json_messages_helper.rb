module JsonMessagesHelper
  def json_model_errors(model, status = 400)
    { json: { errors: model.errors }, status: status }
  end

  def json_service_messages(result, status = 400)
    { json: { error: result.message }, status: status }
  end

  def format_register_messages(hash)
    hash.each do |key, value|
      hash[key] = value.first
    end
  end
end
