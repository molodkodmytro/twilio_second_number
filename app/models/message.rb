# == Schema Information
#
# Table name: messages
#
#  id           :uuid             not null, primary key
#  delivered_at :datetime
#  file         :string
#  message_sid  :string
#  status       :integer          default("sent")
#  text         :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  chat_room_id :uuid
#  phone_id     :uuid
#  user_id      :uuid
#
# Indexes
#
#  index_messages_on_chat_room_id  (chat_room_id)
#  index_messages_on_phone_id      (phone_id)
#  index_messages_on_user_id       (user_id)
#

class Message < ApplicationRecord
  belongs_to :chat_room, touch: true
  belongs_to :user
  belongs_to :phone

  enum status: %I(sent delivered undelivered failed)

  # validates :text, presence: true
  validate :check_content

  mount_uploader :file, MmsUploader

  def capabilities=(hash)
    hash.inject({}) do |memo, (key, value)|
      memo[key.downcase] = value
      memo
    end
  end

  private

  def check_content
    if file.blank? && text.blank?
      errors.add :content, "You can not create blank message"
    end
  end
end
