# == Schema Information
#
# Table name: chat_rooms
#
#  id              :uuid             not null, primary key
#  name            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  first_phone_id  :uuid
#  second_phone_id :uuid
#
# Indexes
#
#  index_chat_rooms_on_first_phone_id   (first_phone_id)
#  index_chat_rooms_on_second_phone_id  (second_phone_id)
#
# Foreign Keys
#
#  fk_rails_...  (first_phone_id => phones.id)
#  fk_rails_...  (second_phone_id => phones.id)
#

class ChatRoom < ApplicationRecord
  # I think there can be belongs to two phone numbers
  # You think right! [ Daniel ]

  has_many :messages, dependent: :destroy

  belongs_to :first_phone, class_name: "Phone", foreign_key: :first_phone_id
  belongs_to :second_phone, class_name: "Phone", foreign_key: :second_phone_id

  validates :first_phone_id, uniqueness: { scope: :second_phone_id }
  validates :second_phone_id, uniqueness: { scope: :first_phone_id }

  def receiver(from_number)
    return second_phone if from_number.eql?(first_phone.number)
    return first_phone  if from_number.eql?(second_phone.number)
    nil
  end

  def sender(from_number)
    return second_phone if from_number.eql?(second_phone.number)
    return first_phone  if from_number.eql?(first_phone.number)
    nil
  end
end
