# == Schema Information
#
# Table name: user_plans
#
#  id            :uuid             not null, primary key
#  spent_minutes :integer          default(0)
#  spent_mms     :integer          default(0)
#  spent_sms     :integer          default(0)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  apple_id      :string
#  plan_id       :uuid
#  user_id       :uuid
#
# Indexes
#
#  index_user_plans_on_plan_id  (plan_id)
#  index_user_plans_on_user_id  (user_id)
#

class UserPlan < ApplicationRecord
  belongs_to :user
  belongs_to :plan

  def left_plan
    {
      left_minutes: self.plan.available_minutes - self.spent_minutes,
      left_sms: self.plan.available_sms - self.spent_sms,
      left_mms: self.plan.available_mms - self.spent_mms
    }
  end
end
