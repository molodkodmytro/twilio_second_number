# == Schema Information
#
# Table name: users
#
#  id          :uuid             not null, primary key
#  udid        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  firebase_id :string
#
# Indexes
#
#  index_users_on_udid  (udid) UNIQUE
#

class User < ApplicationRecord
  has_many :phones

  has_many :user_plans, dependent: :destroy
  has_many :subscriptions, through: :user_plans, foreign_key: :plan_id, source: :plan
  has_many :messages, dependent: :destroy

  def chat_rooms
    ids ||= self.phones.ids
    @chat_rooms = ChatRoom.where(first_phone_id: ids).or(
      ChatRoom.where(second_phone_id: ids)
    )
  end
end
