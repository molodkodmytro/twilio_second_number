# == Schema Information
#
# Table name: countries
#
#  id           :uuid             not null, primary key
#  code         :string
#  country      :string
#  flag_url     :string
#  mobile_code  :string
#  most_popular :boolean          default(FALSE)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Country < ApplicationRecord
  MOST_POPULAR = %W(US CA GB AU FR).freeze

  validates :country, :flag_url, :mobile_code, presence: true
  validates :code, presence: true,
                       uniqueness: { case_sensitive: false }
end
