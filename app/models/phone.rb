# == Schema Information
#
# Table name: phones
#
#  id           :uuid             not null, primary key
#  capabilities :jsonb
#  name         :string
#  number       :string
#  phone_type   :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :uuid
#
# Indexes
#
#  index_phones_on_user_id  (user_id)
#

class Phone < ApplicationRecord
  PHONE_TYPES = [:toll_free, :local, :mobile, :real].freeze
  
  has_many   :messages
  belongs_to :user, optional: true

  with_options dependent: :destroy do |assoc|
    assoc.has_many :from_chat_rooms, class_name: "ChatRoom", foreign_key: "first_phone_id"
    assoc.has_many :to_chat_rooms,   class_name: "ChatRoom", foreign_key: "second_phone_id"
  end

  validates :number, presence: true, uniqueness: { case_sensitive: false }

  def chat_rooms
    ChatRoom.where("first_phone_id = :id OR second_phone_id = :id", id: id)
  end
end
