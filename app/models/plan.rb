# == Schema Information
#
# Table name: plans
#
#  id                :uuid             not null, primary key
#  available_minutes :integer
#  available_mms     :integer
#  available_sms     :integer
#  is_priority       :boolean          default(FALSE)
#  name              :string           default("")
#  time              :integer          default(0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Plan < ApplicationRecord
  has_many :user_plans, dependent: :destroy
  has_many :users, through: :user_plans

  validates :name, presence: true
  validates :available_minutes, presence: true
  validates :available_mms, presence: true
  validates :available_sms, presence: true
end
