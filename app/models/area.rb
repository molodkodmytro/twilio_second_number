# == Schema Information
#
# Table name: areas
#
#  id           :uuid             not null, primary key
#  area_code    :integer
#  country_code :string
#  dialing_code :string
#  name         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Area < ApplicationRecord
  validates :name, :area_code, :dialing_code, :country_code, presence: true
end
