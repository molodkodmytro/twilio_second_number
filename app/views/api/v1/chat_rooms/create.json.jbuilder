json.partial! "chat_room", chat_room: @chat_room
json.messages @chat_room.messages.order(:created_at)