json.chat_rooms @chat_rooms.includes(first_phone: [:user], second_phone: [:user]) do |chat_room|
  json.partial! "chat_room", chat_room: chat_room
end
