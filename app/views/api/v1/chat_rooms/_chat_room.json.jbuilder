json.id          chat_room.id
json.updated_at  chat_room.updated_at

message = chat_room.messages.order(:created_at).last

json.last_message do
  json.partial! "api/v1/messages/message", message: message
end if message.present?

json.members do
  json.array! [chat_room.first_phone, chat_room.second_phone] do |phone|
    json.partial! "api/v1/phones/phone", phone: phone
  end
end