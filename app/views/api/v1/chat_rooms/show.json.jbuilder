json.chat_room do 
  json.partial! "chat_room", chat_room: @chat_room
end
json.messages do
  json.array! @messages.includes(:phone) do |message|
    json.partial! "api/v1/messages/message", message: message
  end
end
