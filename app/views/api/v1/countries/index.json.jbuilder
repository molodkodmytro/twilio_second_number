@countries.each do |key, value|
  json.set! key do 
    json.array! value do |country|
      json.partial! "country", country: country
    end
  end
end