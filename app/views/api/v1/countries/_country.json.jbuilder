expected_locals = %w(country)
expected_locals.each do |local|
  raise("_country partial expects local #{local}") unless binding.local_variable_get(local)
end

json.code           country[:code]
json.country        country[:country]
json.flag_url       country[:flag_url]
json.mobile_code    country[:mobile_code]
json.landline_code  country[:mobile_code].slice(1..-1)
json.has_areas      %W(CA US).include?(country[:code]) ? true : false
