expected_locals = %w(plan)
expected_locals.each do |local|
  raise("_plan partial expects local #{local}") unless binding.local_variable_get(local)
end

json.id                   plan.id
json.available_minutes    plan.available_minutes
json.available_sms        plan.available_sms
json.available_mms        plan.available_mms
json.time                 plan.time
json.created_at           plan.created_at
