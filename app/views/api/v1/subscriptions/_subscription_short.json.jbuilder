expected_locals = %w(subscription)
expected_locals.each do |local|
  raise("_subscription partial expects local #{local}") unless binding.local_variable_get(local)
end

json.id               subscription.id
json.spent_minutes    subscription.spent_minutes
json.spent_sms        subscription.spent_sms
json.spent_mms        subscription.spent_mms
json.left_plan        subscription.left_plan
json.expired_at       subscription.plan.time.zero? ? nil : (subscription.created_at + subscription.plan.time)
