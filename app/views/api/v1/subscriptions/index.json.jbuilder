@plans.each do |key, value|
  json.set! key do 
    json.array! value do |plan|
      json.partial! "plan", plan: plan
    end
  end
end
