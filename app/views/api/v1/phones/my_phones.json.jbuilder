json.array! @phones do |phone|
  json.partial! "phone", phone: phone
end