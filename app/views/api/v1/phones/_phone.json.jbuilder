json.id     phone.id
json.name   phone.name
json.number phone.number
json.capabilities do 
  phone.capabilities.each do |key, value|
    json.set! key, value
  end
end
