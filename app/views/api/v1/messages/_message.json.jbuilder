expected_locals = %w(message)
expected_locals.each do |local|
  raise("_message partial expects local #{local}") unless binding.local_variable_get(local)
end

json.id           message.id
json.text         message.text
json.created_at   message.created_at
json.updated_at   message.updated_at
json.delivered_at message.delivered_at
json.status       message.status
json.chat_room_id message.chat_room_id
json.sender do
  json.partial! "api/v1/phones/phone", phone: message.phone
end
json.file do 
  json.url message.file.url
end