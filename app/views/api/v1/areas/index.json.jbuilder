json.areas @areas.each do |area|
  json.partial! "area", area: area
end