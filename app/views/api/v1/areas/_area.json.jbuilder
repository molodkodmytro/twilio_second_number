json.id            area.id
json.name          area.name
json.region_code   area.area_code
json.dialing_code  @type != 1 ? area.dialing_code : area.dialing_code.slice(1..-1)
