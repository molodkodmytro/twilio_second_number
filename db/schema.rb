# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_26_084113) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "areas", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "country_code"
    t.integer "area_code"
    t.string "dialing_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chat_rooms", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "second_phone_id"
    t.uuid "first_phone_id"
    t.index ["first_phone_id"], name: "index_chat_rooms_on_first_phone_id"
    t.index ["second_phone_id"], name: "index_chat_rooms_on_second_phone_id"
  end

  create_table "countries", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code"
    t.string "country"
    t.string "flag_url"
    t.string "mobile_code"
    t.boolean "most_popular", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "text"
    t.uuid "user_id"
    t.uuid "chat_room_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0
    t.string "file"
    t.string "message_sid"
    t.uuid "phone_id"
    t.datetime "delivered_at"
    t.index ["chat_room_id"], name: "index_messages_on_chat_room_id"
    t.index ["phone_id"], name: "index_messages_on_phone_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "phones", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "number"
    t.integer "phone_type"
    t.uuid "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.jsonb "capabilities", default: {"fax"=>false, "mms"=>false, "sms"=>false, "voice"=>false}
    t.index ["user_id"], name: "index_phones_on_user_id"
  end

  create_table "plans", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", default: ""
    t.boolean "is_priority", default: false
    t.integer "available_minutes"
    t.integer "available_sms"
    t.integer "available_mms"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "time", default: 0
  end

  create_table "user_plans", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "plan_id"
    t.integer "spent_minutes", default: 0
    t.integer "spent_sms", default: 0
    t.integer "spent_mms", default: 0
    t.string "apple_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["plan_id"], name: "index_user_plans_on_plan_id"
    t.index ["user_id"], name: "index_user_plans_on_user_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "udid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "firebase_id"
    t.index ["udid"], name: "index_users_on_udid", unique: true
  end

  add_foreign_key "chat_rooms", "phones", column: "first_phone_id"
  add_foreign_key "chat_rooms", "phones", column: "second_phone_id"
end
