class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages, id: :uuid do |t|
      t.text :text

      t.references :user, type: :uuid
      t.references :chat_room, type: :uuid
      t.timestamps
    end
  end
end
