class AddNameToPhones < ActiveRecord::Migration[5.2]
  def change
    add_column :phones, :name, :string
  end
end
