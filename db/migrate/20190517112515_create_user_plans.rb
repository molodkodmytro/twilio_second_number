class CreateUserPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :user_plans, id: :uuid do |t|
      t.references :user, type: :uuid
      t.references :plan, type: :uuid
      t.integer    :spent_minutes, default: 0
      t.integer    :spent_sms, default: 0
      t.integer    :spent_mms, default: 0
      t.string     :apple_id

      t.timestamps
    end
  end
end
