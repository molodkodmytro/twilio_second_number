class AddMessageSidToMessage < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :message_sid, :string
  end
end
