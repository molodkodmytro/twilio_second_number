class AddCapabilitiesToPhone < ActiveRecord::Migration[5.2]
  def change
    add_column :phones, :capabilities, :jsonb, default: { voice: false, sms: false, mms: false, fax: false }
  end
end
