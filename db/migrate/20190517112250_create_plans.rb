class CreatePlans < ActiveRecord::Migration[5.2]
  def change
    create_table :plans, id: :uuid do |t|
      t.string   :name, default: ""
      t.boolean  :is_priority, default: false
      t.datetime :time
      t.integer  :available_minutes
      t.integer  :available_sms
      t.integer  :available_mms

      t.timestamps
    end
  end
end
