class ChangeTypeTimeOnPlan < ActiveRecord::Migration[5.2]
  def change
    remove_column :plans, :time
    add_column :plans, :time, :integer, default: 0
  end
end
