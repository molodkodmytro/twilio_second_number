class CreateCountries < ActiveRecord::Migration[5.2]
  def change
    create_table :countries, id: :uuid do |t|
      t.string  :code
      t.string  :country
      t.string  :flag_url
      t.string  :mobile_code
      t.boolean :most_popular, default: false

      t.timestamps
    end
  end
end
