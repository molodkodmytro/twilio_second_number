class CreateChatRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :chat_rooms, id: :uuid do |t|
      t.string     :name
      t.references :first_user,  type: :uuid, foreign_key: {to_table: :users}
      t.references :second_user, type: :uuid, foreign_key: {to_table: :users}

      t.timestamps
    end
  end
end
