class AddDeliveredAtToMessage < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :delivered_at, :datetime
  end
end
