class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    enable_extension "pgcrypto"

    create_table :users, id: :uuid do |t|
      t.string :udid

      t.timestamps
    end

    add_index :users, :udid, unique: true
  end
end
