class ChangeChatRoom < ActiveRecord::Migration[5.2]
  def change
    remove_reference :chat_rooms, :second_user, type: :uuid, index: true, foreign_key: { to_table: :users }
    remove_reference :chat_rooms, :first_user, type: :uuid, index: true, foreign_key: { to_table: :users }

    add_reference :chat_rooms, :second_phone, type: :uuid, foreign_key: { to_table: :phones }
    add_reference :chat_rooms, :first_phone, type: :uuid, foreign_key: { to_table: :phones }
  end
end
