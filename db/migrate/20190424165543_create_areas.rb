class CreateAreas < ActiveRecord::Migration[5.2]
  def change
    create_table :areas, id: :uuid do |t|
      t.string  :name
      t.string  :country_code
      t.integer :area_code
      t.string  :dialing_code

      t.timestamps
    end
  end
end
