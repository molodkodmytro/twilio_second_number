class AddFileToMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :file, :string
  end
end
