class AddPhoneIdToMessages < ActiveRecord::Migration[5.2]
  def change
    add_reference :messages, :phone, type: :uuid
  end
end
