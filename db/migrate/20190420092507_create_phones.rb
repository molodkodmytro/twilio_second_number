class CreatePhones < ActiveRecord::Migration[5.2]
  def change
    create_table :phones, id: :uuid do |t|
      t.string     :number
      t.integer    :phone_type
      t.references :user, type: :uuid

      t.timestamps
    end
  end
end
