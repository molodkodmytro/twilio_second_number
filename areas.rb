require 'open-uri'
require 'nokogiri'

@materik = "Europe"
countries = ["Russia", "Germany", "United_Kingdom", "Spain", "France", "Italy", "Ireland", "Portugal", "Sweden", "Turkey", "Austria", "Ukraine", "Poland", "Greece", "Denmark", "Serbia_and_Montenegro", "Netherlands", "Switzerland", "Norway", "Romania", "Finland", "Belgium", "Czech_Republic", "Belarus", "Bosnia_&_Herzegovina", "Hungary", "Bulgaria", "Estonia", "Slovakia", "Iceland", "Croatia", "Latvia", "Lithuania", "Montenegro", "Luxembourg", "Moldova", "Slovenia", "Albania", "Andorra", "Monaco", "Gibraltar", "Liechtenstein"]




# Europe
# countries = ["Russia", "Germany", "United_Kingdom", "Spain", "France", "Italy", "Ireland", "Portugal", "Sweden", "Turkey", "Austria", "Ukraine", "Poland", "Greece", "Denmark", "Serbia_and_Montenegro", "Netherlands", "Switzerland", "Norway", "Romania", "Finland", "Belgium", "Czech_Republic", "Belarus", "Bosnia_&_Herzegovina", "Hungary", "Bulgaria", "Estonia", "Slovakia", "Iceland", "Croatia", "Latvia", "Lithuania", "Montenegro", "Luxembourg", "Moldova", "Slovenia", "Albania", "Andorra", "Monaco", "Gibraltar", "Liechtenstein"]
# Africa
countries << ["South_Africa", "Algeria", "Egypt", "Morocco", "Nigeria", "Tunisia", "Kenya", "Botswana", "Zimbabwe", "Niger", "Libya", "Gabon", "Sudan", "Ethiopia", "Tanzania", "Cameroon", "Mozambique", "Mali", "Namibia", "Mauritius", "Senegal", "Western Sahara", "Mauritania", "Zambia", "Cote d'Ivoire", "Malta", "Madagascar", "Angola", "Central African Rep.", "Somalia", "Benin", "Ghana", "Comoros", "Lesotho", "Equatorial Guinea", "Guinea", "Cape_Verde", "Eritrea", "Reunion", "Uganda", "Malawi", "Liberia", "Sierra_Leone", "Djibouti", "Rwanda", "Swaziland", "Guinea_Bissau", "Gambia", "Burundi", "Burkina_Faso", "Togo"]
# Asia
countries << ["China", "India", "Japan", "Indonesia", "Philippines", "Kazakhstan", "Thailand", "Korea (South)", "Mongolia", "Saudi Arabia", "Vietnam", "Iran", "Israel", "Malaysia", "Taiwan", "Turkmenistan", "Pakistan", "Uzbekistan", "Korea (North)", "Syria", "Iraq", "Bangladesh", "Oman", "Cyprus", "Azerbaijan", "Kyrgyzstan", "Macedonia", "Lebanon", "Yemen", "Jordan", "United Arab Emirates", "Singapore", "Afghanistan", "Sri Lanka", "Myanmar (Burma)", "Nepal", "Georgia", "Hong Kong", "Qatar", "Kuwait", "Armenia", "Bhutan", "Bahrain", "Tajikistan", "Brunei", "Macau", "Maldives", "Laos", "Cambodia"]
# America North
countries << ["Mexico"]
# Central
countries << ["Cuba", "Honduras", "Guatemala", "Costa Rica", "Dominican Republic", "Jamaica", "Nicaragua", "El Salvador", "Panama", "Guadeloupe", "Bahamas", "Netherlands Antilles", "Haiti", "Martinique", "St Lucia", "Trinidad & Tobago", "Belize", "Aruba", "Dominica", "Grenada", "Antigua & Barbuda", "Barbados", "Cayman Islands", "Anguilla", "Bermuda", "St Kitts & Nevis"]
# http://dialcode.org/South_America/
countries << ["Brazil", "Argentina", "Colombia", "Chile", "Venezuela", "Bolivia", "Peru", "Uruguay", "Paraguay", "Ecuador", "Guyana", "Suriname", "French Guiana", "Falkland Islands"]
# http://dialcode.org/Oceania/
countries << ["Australia", "New Zealand", "Fiji", "French_Polynesia", "New Caledonia", "Papua New Guinea", "Vanuatu", "Solomon Islands", "Kiribati", "Tuvalu", "Tonga", "Seychelles", "Nauru"]
# USA




def get_all(country)
  url = "http://dialcode.org/#{@materik}/#{country}/"
  # begin
  html = open(url)
  # rescue => e
  #   return []
  # end

  doc = Nokogiri::HTML(html)

  number = doc.css(".topmenu2 .topmenu2 p").first.text.split(":").last.strip

  table = doc.css('table')

  arr = []
  table.css("tr").slice(1..-1).each do |el|
    tags = el.css("td")
    obj = {
      country_code: number,
      name: tags[0].inner_html,
      area_code: tags[1].inner_html,
      dialing_code: tags[2].inner_html,
    }
    arr << obj
  end
  arr
end

result = []
countries.flatten.each do |country|
  result << get_all(country.split(" ").join("_"))
end
# pp result.flatten.count
result.flatten.each do |obj|
  str = "Area.create(name: \"#{obj[:name]}\", country_code: #{obj[:country_code]}, area_code: #{obj[:area_code]}, dialing_code: \"#{obj[:dialing_code]}\")"
end



# url = "http://dialcode.org/Oceania/"
# html = open(url)
# doc = Nokogiri::HTML(html)
# list = doc.css(".ltopmenu2 ul")
# # list.css("li")
# list.css("li").each do |country|
#   countries << country.text.slice(5..-1)
# end

# countries